package com.captjack.distributedlock.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;

/**
 * 基类
 *
 * @author Jack Sparrow
 * @version 1.0.0
 * @date 2018/7/12 22:11
 * package com.captjack.distributedlock.aspect
 */
class BaseDistributedLock {

    /**
     * 获取注解实例信息
     *
     * @param proceedingJoinPoint 切点信息
     * @param clazz               类对象
     * @param <T>                 范型
     * @return 实例
     */
    <T extends Annotation> T getAnnotation(ProceedingJoinPoint proceedingJoinPoint, Class<T> clazz) {
        return ((MethodSignature) proceedingJoinPoint.getSignature()).getMethod().getAnnotation(clazz);
    }

}
