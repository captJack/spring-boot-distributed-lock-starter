package com.captjack.distributedlock.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * 基于redis实现的分布式锁
 *
 * @author Capt Jack
 * @date 2018/7/12
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
@Inherited
public @interface DistributedLockByRedis {

    /**
     * @return 锁id
     */
    String lockId();

    /**
     * @return 锁自动释放时间设置
     */
    long lockExpireTime() default 3000L;

    /**
     * @return 释放单位设置
     */
    TimeUnit lockExpireTimeUnit() default TimeUnit.MILLISECONDS;

    /**
     * @return 是否开启锁自动超时
     */
    boolean isExpireLock() default true;

}
