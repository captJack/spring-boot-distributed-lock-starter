package com.captjack.distributedlock.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * 基于zookeeper实现的分布式锁
 *
 * @author Capt Jack
 * @date 2018/7/12
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
@Inherited
public @interface DistributedLockByZookeeper {

    /**
     * @return 锁id
     */
    String lockId();

    /**
     * @return 等待获取锁超时时间设置
     */
    long waitLockTime() default 3000L;

    /**
     * @return 超时单位设置
     */
    TimeUnit waitLockTimeUnit() default TimeUnit.MILLISECONDS;

    /**
     * @return 是否等待获取锁直到超时
     */
    boolean isWaitLockUntilTimeOut() default true;

}
