package com.captjack.distributedlock.annotation;

import org.springframework.context.annotation.ComponentScan;

/**
 * 开启分布式锁
 *
 * @author Capt Jack
 * @date 2018/7/13
 */
@ComponentScan(basePackages = {"com.captjack.distributedlock"})
public class AutoConfigurationDistributedLock {

}
