package com.captjack.distributedlock.lock;

import java.util.concurrent.TimeUnit;

/**
 * 分布式锁接口
 *
 * @author Jack Sparrow
 * @version 1.0.0
 * @date 2018/7/12 21:37
 * package com.captjack.distributedlock.lock
 */
public interface ZookeeperDistributedLock {

    /**
     * 尝试加锁
     *
     * @param lockId                 锁id
     * @param waitLockTime           等待时间，防止出现死锁
     * @param waitLockTimeUnit       等待时间单位
     * @param isWaitLockUntilTimeOut 是否等待锁直到超时
     * @return 是否拿到锁
     * @throws Exception 异常
     */
    boolean acquire(String lockId, long waitLockTime, TimeUnit waitLockTimeUnit, boolean isWaitLockUntilTimeOut) throws Exception;

    /**
     * 解锁
     *
     * @param lockId 锁id
     * @return 是否解锁成功
     * @throws Exception 异常
     */
    boolean release(String lockId) throws Exception;

}
