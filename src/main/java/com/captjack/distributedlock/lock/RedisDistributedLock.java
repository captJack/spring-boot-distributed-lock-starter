package com.captjack.distributedlock.lock;

import java.util.concurrent.TimeUnit;

/**
 * 分布式锁接口
 *
 * @author Jack Sparrow
 * @version 1.0.0
 * @date 2018/7/12 21:37
 * package com.captjack.distributedlock.lock
 */
public interface RedisDistributedLock {

    /**
     * 尝试加锁
     *
     * @param lockId             锁id
     * @param lockExpireTime     锁自动释放时间，防止出现死锁
     * @param lockExpireTimeUnit 时间单位
     * @param isExpireLock       是否设置锁自动超时释放
     * @param requestId          请求id
     * @return 是否拿到锁
     * @throws Exception 异常
     */
    boolean acquire(String lockId, String requestId, long lockExpireTime, TimeUnit lockExpireTimeUnit, boolean isExpireLock) throws Exception;

    /**
     * 解锁
     *
     * @param lockId    锁id
     * @param requestId 请求id
     * @return 是否解锁成功
     * @throws Exception 异常
     */
    boolean release(String lockId, String requestId) throws Exception;

}
