package com.captjack.distributedlock.lock.redis;

import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.scripting.support.StaticScriptSource;

import java.nio.charset.Charset;

/**
 * Description description.....
 *
 * @author Jack Sparrow
 * @version 1.0.0
 * @date 2018/7/15 18:57
 * package com.captjack.distributedlock.lock.redis
 */
class RedisDistributedLockConstant {

    /**
     * 默认使用u8编码格式
     */
    static final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    private RedisDistributedLockConstant() {

    }

}
