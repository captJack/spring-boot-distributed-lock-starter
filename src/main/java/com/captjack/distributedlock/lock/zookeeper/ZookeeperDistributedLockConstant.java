package com.captjack.distributedlock.lock.zookeeper;

import org.apache.curator.framework.recipes.locks.InterProcessLock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Capt Jack
 * @date 2018/7/13
 */
public interface ZookeeperDistributedLockConstant {

    /**
     * 分布式锁存储MAP，避免重复创建对象
     */
    Map<String, InterProcessLock> INTER_PROCESS_LOCK_MAP = new ConcurrentHashMap<>();

}
