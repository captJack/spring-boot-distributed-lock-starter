local values = redis.call('get', KEYS[1]);
if values == nil then
    return -1;
elseif values == ARGV[1] then
    return redis.call('del', KEYS[1]);
else
    return 2;
end